package model.vo;

public class ServicioResumen 
{
	private String company;
	private String trip_end_timestamp;
	private String trip_start_timestamp;
	private Taxi taxi;
	
	public ServicioResumen(String Company, String trip_start_timestamp , String trip_end_timestamp, Taxi taxi)
	{
		this.company=Company;
		this.trip_end_timestamp=trip_end_timestamp;
		this.trip_start_timestamp=trip_start_timestamp;
		this.setTaxi(taxi);
	}
	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}
	/**
	 * @return the trip_end_timestamp
	 */
	public String getTrip_end_timestamp() {
		return trip_end_timestamp;
	}
	/**
	 * @param trip_end_timestamp the trip_end_timestamp to set
	 */
	public void setTrip_end_timestamp(String trip_end_timestamp) {
		this.trip_end_timestamp = trip_end_timestamp;
	}
	/**
	 * @return the trip_start_timestamp
	 */
	public String getTrip_start_timestamp() {
		return trip_start_timestamp;
	}
	/**
	 * @param trip_start_timestamp the trip_start_timestamp to set
	 */
	public void setTrip_start_timestamp(String trip_start_timestamp) {
		this.trip_start_timestamp = trip_start_timestamp;
	}
	/**
	 * @return the taxi
	 */
	public Taxi getTaxi() {
		return taxi;
	}
	/**
	 * @param taxi the taxi to set
	 */
	public void setTaxi(Taxi taxi) {
		this.taxi = taxi;
	}
	
	
}
