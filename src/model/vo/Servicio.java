package model.vo;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	
	private String trip_id;
	private String taxi_id;
	private String trip_seconds;
	private String trip_miles;
	private String trip_total;
	private String trip_end_timestamp;
	private String trip_start_timestamp;
	
	
	public Servicio(String trip_id, String taxi_id, String trip_seconds, String trip_miles, String trip_total, String trip_start_timestamp, String trip_end_timestamp)
	{
		this.trip_id=trip_id;
		this.taxi_id=taxi_id;
		this.trip_seconds=trip_seconds;
		this.trip_miles=trip_miles;
		this.trip_total=trip_total;
		this.trip_start_timestamp=trip_start_timestamp;
		this.trip_end_timestamp=trip_end_timestamp;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		return Integer.parseInt(trip_seconds);
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() 
	{
		return Double.parseDouble(trip_miles);
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() 
	{
		return Double.parseDouble(trip_total);
	}

	@Override
	public int compareTo(Servicio arg0) 
	{
		return 0;
	}
	/**
	 * @return the trip_end_timestamp
	 */
	public String getTrip_end_timestamp() {
		return trip_end_timestamp;
	}
	/**
	 * @param trip_end_timestamp the trip_end_timestamp to set
	 */
	public void setTrip_end_timestamp(String trip_end_timestamp) {
		this.trip_end_timestamp = trip_end_timestamp;
	}
	/**
	 * @return the trip_start_timestamp
	 */
	public String getTrip_start_timestamp() {
		return trip_start_timestamp;
	}
	/**
	 * @param trip_start_timestamp the trip_start_timestamp to set
	 */
	public void setTrip_start_timestamp(String trip_start_timestamp) {
		this.trip_start_timestamp = trip_start_timestamp;
	}
}
