package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	
	
	private String taxi_id;
	private String company;
	
	public Taxi(String taxi_id, String company)
	{
		this.taxi_id=taxi_id;
		this.company=company;
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany()
	{
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) 
	{
		return 0;
	}	
}