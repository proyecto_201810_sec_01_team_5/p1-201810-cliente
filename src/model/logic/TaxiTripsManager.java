package model.logic;

import java.io.BufferedReader;  
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.data_structures.Cola;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.Node;
import model.data_structures.Pila;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServicioGenerico;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	//Estructuras
	Cola<ServicioGenerico> colaServiciosPorTaxi=new Cola<ServicioGenerico>();
	Pila<ServicioGenerico> pilaServiciosPorTaxi=new Pila<ServicioGenerico>();
	ServicioGenerico[] serviciosGenericos=null;
	ListaDoblementeEncadenada<Servicio> servicios=null;
	ListaDoblementeEncadenada<ServicioResumen> ListaServiciosResumen=null;
	ListaDoblementeEncadenada<InfoTaxiRango> ListaInfoTaxiRango=null;
	Taxi taxi;
	Servicio servicio;
	ZonaServicios zona;
	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		BufferedReader reader=null;

		try
		{
			reader=new BufferedReader(new FileReader(direccionJson));
			Gson gson=new GsonBuilder().create();
			ServicioGenerico[] serviciosGenericos=gson.fromJson(reader, ServicioGenerico[].class);
			serviciosGenericos=ordenarArregloPorFecha();
			System.out.println("object mode: " + serviciosGenericos[0].getTrip_seconds()); //La informaci�n se sube a un solo arreglo con objetos de servicios gen�ricos

			for(int i=0; i<serviciosGenericos.length;i++)
			{
				if(serviciosGenericos[i]!=null)
				{	
					colaServiciosPorTaxi.enqueue(serviciosGenericos[i]);
					pilaServiciosPorTaxi.push(serviciosGenericos[i]);
				}

				//Creaci�n ArrayList de clase Servicio
				if(serviciosGenericos[i]!=null)
				{	
					ServicioGenerico actual=serviciosGenericos[i];
					Servicio nuevoServicio=new Servicio(actual.getTrip_id(), actual.getTaxi_id(), actual.getTrip_seconds(), actual.getTrip_miles(), actual.getTrip_total(), actual.getTrip_start_timestamp(), actual.getTrip_end_timestamp());
					servicios.add(nuevoServicio);
				}

				//Creaci�n ArrayList de clase ServicioResumen
				if(serviciosGenericos[i]!=null)
				{	
					ServicioGenerico actual=serviciosGenericos[i];
					Taxi taxiActual=new Taxi(actual.getTaxi_id(),actual.getCompany());
					ServicioResumen nuevoServicioResumen=new ServicioResumen(actual.getCompany(), actual.getTrip_start_timestamp(), actual.getTrip_end_timestamp(), taxiActual);					
					ListaServiciosResumen=new ListaDoblementeEncadenada<ServicioResumen>();
					ListaServiciosResumen.add((Comparable<ServicioResumen>) nuevoServicioResumen);
				}

				//Creaci�n ArrayList de clase InfoTaxiRango
				if(serviciosGenericos[i]!=null)
				{	
					ServicioGenerico actual=serviciosGenericos[i];
					InfoTaxiRango nuevoInfoTaxiRango=new InfoTaxiRango(actual.getTaxi_id(), actual.getCompany(), 0, 0,"");
					ListaInfoTaxiRango=new ListaDoblementeEncadenada<InfoTaxiRango>();
					ListaInfoTaxiRango.add((Comparable<InfoTaxiRango>) nuevoInfoTaxiRango);
				}
			}			
		}
		catch(FileNotFoundException e)
		{
			System.out.println("No se encuentra el archivo");
		}
		finally
		{
			System.out.println("Listo!");
		}

		return false;
	}

	@Override //1A
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		Cola<Servicio> res=new Cola<Servicio>();
		ListaDoblementeEncadenada<Servicio> aux= servicios;
		Node<Servicio> actual=aux.getHead();
		//Compara cada elemento del arreglo y lo agrega a la cola
		while(actual!=null)
		{
			if(actual.getElement().getTrip_start_timestamp().compareTo(rango.getFechaInicial().concat(rango.getHoraInicio()))>=0 && actual.getElement().getTrip_end_timestamp().compareTo(rango.getFechaFinal().concat(rango.getHoraFinal()))<=0)
			{
				res.enqueue(actual.getElement());
				actual=actual.getNext();
			}
		}
		return res;
	}

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		Taxi res=null;

		//Crea la lista auxiliar de comparaci�n
		ListaDoblementeEncadenada<ServicioResumen> listaAuxiliar=new ListaDoblementeEncadenada<ServicioResumen>();
		Node<ServicioResumen> nodoLista=ListaServiciosResumen.getHead();
		while(nodoLista!=null)
		{
			if(nodoLista.getElement().getCompany().equals(company) && nodoLista.getElement().getTrip_start_timestamp().compareTo(rango.getFechaInicial().concat(":"+rango.getHoraInicio()))>=0 && nodoLista.getElement().getTrip_end_timestamp().compareTo(rango.getFechaFinal().concat(":"+rango.getHoraFinal()))<=0)
			{
				listaAuxiliar.add((Comparable) nodoLista);
			}
			nodoLista=nodoLista.getNext();
		}

		//Compara cada aparici�n de la lista encadenada y devuelve el que m�s veces apareci�
		Node<ServicioResumen> nodoActual=listaAuxiliar.getHead();
		int ocurrenciasMax=0;
		Taxi max=null;
		while(nodoActual!=null)
		{
			String taxiId=nodoActual.getElement().getTaxi().getTaxiId();
			int ocurrenciasActual=1;
			Node<ServicioResumen> nodoComparar=nodoActual.getNext();
			while(nodoComparar!=null)
			{
				if(nodoActual.getNext().getElement().getTaxi().getTaxiId().equals(taxiId))
				{
					ocurrenciasActual++;
					if(ocurrenciasActual>ocurrenciasMax)
					{
						max=nodoActual.getElement().getTaxi();
						ocurrenciasMax=ocurrenciasActual;
					}
				}
				nodoComparar=nodoComparar.getNext();
			}
		}

		res=max;
		return res;
	}

	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		//Establece los campos
		InfoTaxiRango res=new InfoTaxiRango(id, "",0,0,"");
		LinkedList<Servicio> serviciosPrestadosEnRango=new ListaDoblementeEncadenada<Servicio>();
		double plataGanada=0;
		double distanciaTotalRecorrida=0;
		String tiempoTotal="";
		Cola<ServicioGenerico> auxiliar=colaServiciosPorTaxi;
		ServicioGenerico actual=auxiliar.dequeue();
		//Llena el campo id y rango
		res.setIdTaxi(id);
		res.setRango(rango);
		//Llena el campo Company
		while(actual!=null)
		{
			if(actual.getTaxi_id().equals(id))
			{
				res.setCompany(actual.getCompany());
			}
			actual=auxiliar.dequeue();
		}
		//Llena los campos serviciosPrestadosEnRango, plataGanada, distanciaTotalRecorrida y tiempoTotal
		while(actual!=null)
		{
			if(actual.getTaxi_id().equals(id) && actual.getTrip_start_timestamp().compareTo(rango.getFechaInicial().concat(":"+rango.getHoraInicio()))>=0 && actual.getTrip_end_timestamp().compareTo(rango.getFechaFinal().concat(":"+rango.getHoraFinal()))<=0)
			{
				Servicio nuevoServicio=new Servicio(actual.getTrip_id(), actual.getTaxi_id(),actual.getTrip_seconds(),actual.getTrip_miles(),actual.getTrip_total(), actual.getTrip_start_timestamp(),actual.getTrip_end_timestamp());
				serviciosPrestadosEnRango.add(nuevoServicio);
				plataGanada+=Double.parseDouble(actual.getTrip_total());
				distanciaTotalRecorrida+=Double.parseDouble(actual.getTrip_miles());
				tiempoTotal+=Double.parseDouble(actual.getTrip_seconds());
			}
			actual=auxiliar.dequeue();
		}
		res.setDistanciaTotalRecorrida(distanciaTotalRecorrida);
		res.setPlataGanada(plataGanada);
		res.setTiempoTotal(tiempoTotal);
		res.setServiciosPrestadosEnRango(serviciosPrestadosEnRango);
		return res;
	}

	@Override //4A
	public LinkedList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		//Instancia los par�metros a utilizar
		LinkedList<RangoDistancia> res=new ListaDoblementeEncadenada<RangoDistancia>();
		LinkedList<RangoDistancia> aux=servicios;
		Node<Servicio> actual=servicios.getHead();

		//Encuentra el maximo de millas
		double max=0;
		while(actual!=null)
		{
			double millasActual=actual.getElement().getTripMiles();
			if(millasActual>max)
			{
				max=millasActual;
			}
			actual=actual.getNext();
		}

		//Itera hasta el m�ximo de millas
		for(int i=1; i<=Math.ceil(max); i++)
		{
			int techo=i;
			int piso=i-1;
			LinkedList<Servicio> ListaRango=new ListaDoblementeEncadenada<Servicio>(); 
			while(actual!=null)
			{
				if(actual.getElement().getTrip_start_timestamp().compareTo(fecha+":"+horaInicial)>=0 && actual.getElement().getTrip_end_timestamp().compareTo(fecha+":"+horaInicial)<=0)
				{
					if(actual.getElement().getTripMiles()>=piso && actual.getElement().getTripMiles()<techo)
					{
						ListaRango.add(actual.getElement());
					}
				}	
				actual=actual.getNext();
			}
			RangoDistancia rangoActual=new RangoDistancia(techo, piso, ListaRango);
			res.add(rangoActual);
		}

		return res;
	}

	@Override //1B
	public LinkedList<Compania> darCompaniasTaxisInscritos() 
	{
		return null;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		Compania compañia = new Compania();
		if(compañia == null)
		{
			System.err.println("La compañia no existe");
			return null;
		}

		String inicio= rango.getFechaInicial()+ "T" +rango.getHoraInicio();
		String fin= rango.getFechaFinal()+ "T" +rango.getHoraFinal();

		Servicio compararRango = new Servicio( servicio.getTripId(), taxi.getTaxiId(), servicio.getTrip_start_timestamp(), servicio.getTrip_end_timestamp(), servicio.getTripMiles(), servicio.getTripTotal());

		Taxi mejor = null;
		for(int i = 0; i < servicios.size(); i++)
		{
			Taxi taxi = compañia.taxisInscritos.get(1);
			double max = 0;
			double contador =0;

			for (int j = 0; j < servicios.size(); j++)
			{
				System.out.println(i + "-" + j);
				Servicio serv = servicios.get(j);

				if(serv.getTrip_end_timestamp().after(compararRango.getTrip_end_timestamp()))
					break;

				if(!serv.getTrip_start_timestamp().before(compararRango.getTrip_start_timestamp()))
				{
					System.out.println("2aaa");
					contador += servicio.trip_total;
				}	
			}
			if(contador > max)
			{
				max = contador;
				mejor = taxi;
			}
		}
		return mejor;
	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		ServiciosValorPagado[] arreglo = new ServiciosValorPagado[3];
		arreglo[0] = new ServiciosValorPagado();
		arreglo[1] = new ServiciosValorPagado();
		arreglo[2] = new ServiciosValorPagado();

		String primera = rango.getFechaInicial()+ "T" +rango.getHoraInicio();
		String segunda = rango.getFechaFinal()+ "T" +rango.getHoraFinal();
		int zona = Integer.parseInt(idZona);

		Servicio acomparar = new Servicio(servicio.getTripId(), taxi.getTaxiId(), servicio.getTrip_start_timestamp(), servicio.getTrip_end_timestamp(), servicio.getTripMiles(), servicio.getTripTotal());
		servicios.listing();

		while(true)
		{
			Servicio servicio = servicios.getCurrent();

			if(!servicio.getTrip_end_timestamp().before(acomparar.getTrip_end_timestamp()))
				break;

			if(servicio.compareTo(acomparar) >= 0)
			{
				arreglo[0].getServiciosAsociados().add(servicio);
				arreglo[0].setValorAcumulado(arreglo[0].getValorAcumulado() + servicio.getTripTotal());
			}
			servicios.next();
			try 
			{
				servicios.getCurrent();
			} 
			catch (NullPointerException e){
			}
		}
	}

	@Override //4B
	public LinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		LinkedList<ZonaServicios> lista = LinkedList<ZonaServicios>();

		String inicio= rango.getFechaInicial()+ "T" +rango.getHoraInicio();
		String fin= rango.getFechaFinal()+ "T" +rango.getHoraFinal();

		Servicio acomparar = new Servicio(servicio.getTripId(), taxi.getTaxiId(), servicio.getTrip_start_timestamp(), servicio.getTrip_end_timestamp(), servicio.getTripMiles(), servicio.getTripTotal());

		servicios.listing();

		while(true)
		{
			Servicio servicio = servicios.getCurrent();

			if(!servicio.getTrip_end_timestamp().before(acomparar.getTrip_end_timestamp()))
				break;
			if(servicio.compareTo(acomparar) >= 0)
			{
				ZonaServicios temporal = lista.get(new ZonaServicios("" + zona.getIdZona()));
				if(temporal == null) temporal = lista.addInOrder(new ZonaServicios(""+ zona.getIdZona()));
				temporal.getFechasServicios().add(servicio);
			}
			servicios.next();
			try 
			{
				servicios.getCurrent();
			} 
			catch (NullPointerException e) 
			{
				break;
			}
		}
		return lista;
	}

	@Override //2C
	public LinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		LinkedList<CompaniaServicios> respuesta = LinkedList<CompaniaServicios>();
		LinkedList<Compania> companias = LinkedList<Compania> ();
		int superior = 1;
		int inferior = 0;
		
		for(int i=0;i<companias.size();i++)
		{
			LinkedList<Servicio> servicios = companias.get(i).serviciosEnRango(rango);
			
			if(servicios.size()>=superior || servicios.size()>inferior)
			{
				CompaniaServicios agregar = new CompaniaServicios();
				agregar.setNomCompania(companias.get(i).getNombre());
				agregar.setServicios(servicios);
				respuesta.add(agregar);
				
				if(respuesta.size()>n)
				{
					respuesta.delete(respuesta.get(n));
				}
				
				superior = respuesta.get(0).getServicios().size();
				inferior = respuesta.get(n).getServicios().size();
			}
		}
		return respuesta;
	}

	@Override //3C
	public LinkedList<CompaniaTaxi> taxisMasRentables()
	{
		LinkedList<CompaniaTaxi> masRentables = LinkedList<CompaniaTaxi>();
		
		for (int i = 0; i < servicios.size(); i++) 
		{
			CompaniaTaxi agregar = new CompaniaTaxi();companiasMasServicios(servicio.getTrip_start_timestamp(), servicios.size() - 1);
			Compania compañia = compañia.servicioList.get(i);
			agregar.setNomCompania(compañia.getNombre());
			masRentables.add(agregar);
		}
		
		return masRentables;
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		double valorLimite=10;
		double sumaMillas=0;
		double sumaDuracion=0;
		double sumaPagos=0;
		String fechaInicial="";
		String FechaFinal="";
		Pila<Servicio> res=new Pila<Servicio>();
		ListaDoblementeEncadenada<Servicio> aux= servicios;
		Node<Servicio> actual=aux.getHead();

		//Carga la fecha inicial de la pila
		while(actual!=null)
		{
			if(actual.getElement().getTaxiId().equals(taxiId) && actual.getElement().getTrip_start_timestamp().compareTo(fecha+":"+horaInicial)>=0 && actual.getElement().getTrip_end_timestamp().compareTo(fecha+":"+horaInicial)<=0)
			{
				fechaInicial=actual.getElement().getTrip_start_timestamp();
			}
			actual=actual.getNext();
		}

		//Compara cada elemento del arreglo y lo agrega a la cola
		while(actual!=null)
		{
			if(actual.getElement().getTaxiId().equals(taxiId) && actual.getElement().getTrip_start_timestamp().compareTo(fecha+":"+horaInicial)>=0 && actual.getElement().getTrip_end_timestamp().compareTo(fecha+":"+horaInicial)<=0)
			{
				if(sumaMillas<valorLimite)
				{
					res.push(actual.getElement());
					FechaFinal=actual.getElement().getTrip_start_timestamp();
					sumaMillas+=actual.getElement().getTripMiles();
					sumaDuracion+=actual.getElement().getTripSeconds();
					sumaPagos+=actual.getElement().getTripTotal();
				}
				else
				{
					Servicio reemplazo= new Servicio("",taxiId, String.valueOf(sumaDuracion), String.valueOf(sumaMillas),String.valueOf(sumaPagos),fechaInicial,FechaFinal);
					while(actual!=null)
					{
						res.pop();
						actual=actual.getNext();
					}
					res.push(reemplazo);
				}

			}
			actual=actual.getNext();
		}
		return res;


	}

	//Metodos auxiliares
	public ServicioGenerico[] ordenarArregloPorFecha()
	{
		ServicioGenerico[] res= serviciosGenericos;
		boolean medidor=false;

		while(medidor!=true)
		{
			for(int i=0; i<res.length;i++)
			{
				if(res[i]!=null && res[i+1]!=null)
				{
					if(res[i].getTrip_start_timestamp().compareTo(res[i+1].getTrip_start_timestamp())>0)
					{
						ServicioGenerico aux=res[i];
						res[i]=res[i+1];
						res[i+1]=aux;
						medidor=false;
					}
					else
					{
						medidor=true;
					}
				}
			}
		}


		return res;
	}

}
