package model.data_structures;

import java.util.ArrayList;

public class Pila<T> implements IStack<T>
{
	private ArrayList<T> pila = null;
	public Pila ()
	{
		pila=new ArrayList<T>();
	}
	
	public void push(T item) 
	{
		pila.add(item);
	}

	
	public T pop() 
	{
		if(pila.get(pila.size())==null)
		{
			return null;
		}
		else
		{
			T res= pila.get(pila.size());
			pila.remove(pila.size());
			return res;
		}
	}

	
	public boolean isEmpty() 
	{
		if(pila.isEmpty())
			return true;
		else
			return false;
	}

}
