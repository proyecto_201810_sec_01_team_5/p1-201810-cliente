package model.data_structures;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

public class ListaApoyo <E extends Comparable<E>, T> implements LinkedList<E>
{
	protected int size;
	protected Node<E> primero;
	protected Node<E> ultimo;
	protected Node<E> actual;

	public ListaApoyo(){
		actual = new Node<E>(null, actual);
		primero = null;
		ultimo = null;
	}
	
	/**
	 * Agrega un alemento a la lista 
	 * NO agrega elementos repetidos
	 * @return 
	 */
	@Override
	public void add(E element) 
	{
		
		if(get(element) != null)
		{
			return;
		}
		
		if(actual == null)
		{
			actual = new Node<E>(element, actual);
		}
		
		if(size == 0)
		{
			primero = actual;
			ultimo = actual;
		}
		else
		{
			ultimo.setNext(actual);
		}
		
		actual.setElement(element);
		size++;
		actual.getElement();
	}
	
	
	
	public void delete(E element) 
	{
		if(size() == 0)
		{
			return;
		}
		else
		{
			if(primero.getElement().compareTo(element) == 0)
			{
				if(primero.getNext() == null)
				{
					primero.setNext(null);
					size--;
				}
				else
				{
					primero = primero.getNext();
					size--;
				}
			}
			listing();
			
			while(actual.getNext()!= null)
			{
				if(actual.getNext().getElement().compareTo(element) == 0)
					break;
				actual = actual.getNext();
			}
			
			if(actual.getNext()==null)
			{
				return;
			}
			else
			{
				actual.setNext(actual.getNext().getNext());
				size--;
			}
		}
	}

	
	
	public E trade()
	{
		E este = primero.getElement();
		primero = primero.getNext();
		size--;
		
		return este;
	}

	public Node merge(Node<E> uno, Node<E> dos)
	{
		if (uno == null) 
			return dos;
	    
		if (dos == null) 
			return uno;

	    if (uno.getElement().compareTo(dos.getElement()) < 0) 
	    {
	        uno.setNext(merge(uno.getNext(), dos));
	        return uno;
	    } 
	    else 
	    {
	        dos.setNext(merge(dos.getNext(), uno));
	        return dos;
	    }
	}
	
	public void avanzar()
	{
		actual = actual.getNext();
	}
	
	public int size() 
	{
		return size;
	}

	
	@Override
	public void listing() {
		
		actual = primero;
	}

	@Override
	public Node<E> getCurrent() {
		
		return (Node<E>) actual.getElement();
	}
	@Override
	public Node<E> next() {
		
		return actual.getNext();
	}

	@Override
	public Node<E> previous() {
		
		return null;
	}

	@Override
	public Node<E> getHead() {
		
		return null;
	}

	@Override
	public Node<E> getTail() {
		
		return null;
	}

	@Override
	public Node<E> get(E t) {
		// TODO Auto-generated method stub
		return null;
	}
}