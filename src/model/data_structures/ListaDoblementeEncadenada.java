package model.data_structures;

public class ListaDoblementeEncadenada<E> implements LinkedList
{
	private Node<E> head=null;
	private Node<E> tail=null;
	private Node<E> actual=null;
	private int size=0;
	
	public ListaDoblementeEncadenada(){}

	public void add(Comparable t) 
	{
		Node<E> newest=new Node( t,null);
		if(size==0)
		{
			head=newest;
		}
		else
		{
			tail.setNext(newest);
			newest.setPrevious(tail);
		}
		tail=newest;
		size++;
	}

	
	public void delete(Comparable t) 
	{
		if(size!=0)
		{
			Node<E> actual=head;
			if (actual.getNext().equals(t))
			{
				Node<E> toDelete=actual.getNext();
				Node<E> next=toDelete.getNext();
				
				if(next!=null)
				{
					actual.setNext(next);
				}
				else
				{
					actual.setNext(null);
				}
			}
			else
			{
				actual=actual.getNext();
			}
		}
		
	}

	
	public Node<E> get(Comparable t) 
	{
		Node<E> result= null;
		if(size!=0)
		{
			Node<E> actual=head;
			if (actual.equals(t))
			{
				result = actual;
			}
			else
			{
				actual=actual.getNext();
			}
		}
		
		return result;
	}

	
	public int size() 
	{
		return size;
	}

	
	public void listing() 
	{
		actual=head;
	}
	
	
	public Node getCurrent() 
	{
		return actual;
	}

	
	public Node next() 
	{
		Node<E> result=null;
		if(actual.getNext()!=null)
		{
			result=actual.getNext();
		}
		
		return result;
	}

	
	public Node previous() 
	{
		Node<E> result=null;
		if(actual.getPrevious()!=null)
		{
			result=actual.getPrevious();
		}
		
		return result;
	}

	public Node getHead() 
	{
		return head;
	}

	public Node getTail() 
	{
		return tail;
	}

	
}
