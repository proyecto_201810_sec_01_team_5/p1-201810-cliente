package model.data_structures;

import java.util.ArrayList;

public class Cola<T> implements IQueue<T>
{
	private ArrayList<T> cola=null;
	
	public Cola()
	{
		cola=new ArrayList<T>();
	}
	
	public void enqueue(T item) 
	{
		cola.add(item);
	}

	public T dequeue() 
	{
		T res=null;
		
		if(cola.get(0)!=null)
		{
			res=cola.get(0);
			cola.remove(0);
		}
		return res;
	}

	
	public boolean isEmpty() 
	{
		if(cola.isEmpty())
			return true;
		else
			return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

}
